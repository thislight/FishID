import 'dart:async';
import '../models/user.dart';
import 'package:quiver/core.dart';
import 'package:webart/web.dart';


Future LoginAPI(Request req) async{
    await req.on("post", (Request req) async{
        var json = await req.asJson() as Map<String,dynamic>;
        Optional<User> u = await User.login(json['user'],json['password']);
        var back = <String,dynamic>{};
        if (u.isPresent){
            var user = u.value;
            var msg = json['message'];
            if (msg == null) msg = "No description";
            var t = await user.createToken(msg);
            back['token'] = t.asMap();
            back['ok'] = true;
        } else {
            back['ok'] = false;
            back['err'] = 1;
        }
        req.res.ok(back);
    });
}
