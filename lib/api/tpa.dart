import 'dart:async';
import 'package:webart/web.dart';
import '../utils/auth.dart';
import '../models/token.dart';
import '../models/tpa.dart';
import '../models/user.dart';
import '../models/permission.dart';


bool _checkIfHasUnexceptKey(Map data,List exceptKeys){
    for (var key in data.keys){
        if (!exceptKeys.contains(key)) return true;
    }
    return false;
}


Future ThirdPartyAppAPI(Request req) async{
    await req.on("post", (Request req) async{
        var json = req.asJson();
        var token = json['token'];
        if (await tokenCanUse(req, token)){
            String appSymbol = req.context("urlparam")['appsymbol'];
            String method = req.context("urlparam")['method'];
            Token t = (await Token.byToken(token)).value;
            User user = (await User.byUsername(t.username)).value;
            var tpa = (await ThirdPartyApp.bySymbol(appSymbol));
            PermissionGroup pAppPG;
            if (t.parentApplicationId == 0){
                pAppPG = PG_FISHID(user.username);
            } else {
                var a = await ThirdPartyApp.byId(t.parentApplicationId);
                if (a.isPresent){
                    pAppPG = await user.p(a.value.symbol);
                } else {
                    giveJSONError(req, 6);
                    return;
                } // ParentAppID not ref a avaliable app
            }

            switch(method){
                case "create":
                    if (!(pAppPG.can("ThirdPartyApp:create") || pAppPG.can("ThirdPartyApp:all"))){
                        giveJSONError(req, -816);
                        break;
                    }
                    if (tpa.isPresent){
                        giveJSONError(req, 1);
                        break;
                    } // 1: the symbol have been used
                    var callback = json['callback'];
                    var permissions = json['permissions'];
                    var name = json['name'];
                    var symbol = json['symbol'];
                    if ((await ThirdPartyApp.byName(name)).isPresent){
                        giveJSONError(req, 2);
                        break;
                    } // 2: the name have been used
                    var tpa = await ThirdPartyApp.create(name, symbol, user.username, callback['method']);
                    await tpa.updateCallbackUrl(callback['url']);
                    await tpa.updatePermission(permissions);
                    req.res.ok({
                        'ok': true,
                        'appid':tpa.id,
                        'name': name,
                        'symbol': symbol,
                    });
                    break;
                case "update":
                    if (!(pAppPG.can("ThirdPartyApp:update") || pAppPG.can("ThirdPartyApp:all"))){
                        giveJSONError(req, -816);
                        break;
                    }
                    if (!tpa.isPresent){
                        giveJSONError(req, 3);
                        break;
                    } // 3: the app not found
                    var app = tpa.value;
                    if (app.owner != user.username){
                        giveJSONError(req, 5);
                        break;
                    } // 5: user not owner of app
                    Map data = json['data'];
                    if (data==null ||
                        data.length<=0 ||
                        _checkIfHasUnexceptKey(data,['callbackUrl','callbackMethod','permissions'])){
                        giveJSONError(req, 4);
                        break;
                    } // 4: the data could not accepted
                    for (var key in data.keys){
                        var val = data[key];
                        switch(key){
                            case "callbackUrl":
                                app.updateCallbackUrl(val);
                                break;
                            case "callbackMethod":
                                app.updateCallbackMethod(val);
                                break;
                            case "permissions":
                                app.updatePermission(val);
                                break;
                        }
                    }
                    req.res.ok({
                        'ok': true,
                    });
                    break;
                case "infomation":
                    if (!(pAppPG.can("ThirdPartyApp:infomation") || pAppPG.can("ThirdPartyApp:all"))){
                        giveJSONError(req, -816);
                        break;
                    } // -816 : Permission error
                    if (!tpa.isPresent){
                        // Return app list
                        var apps = await ThirdPartyApp.byOwner(user.username);
                        req.res.ok({
                            'ok':true,
                            'apps': apps.map((app) => app.asMap()).toList(),
                        });
                    } else {
                        var app = tpa.value;
                        if (app.owner != user.username){
                            giveJSONError(req, 5);
                            break;
                        }
                        req.res.ok({
                            'ok': true,
                            'app': app.asMap(),
                        });
                    }
                    break;
                default:
                    req.res.notFound();
                    break;
            }
        }
    });
}
