import 'dart:async';
import 'package:webart/web.dart';
import '../utils/auth.dart';
import '../models/tpa.dart';
import '../models/token.dart';
import '../models/user.dart';
import '../models/permission.dart';


Future AppAuthInfoAPI(Request req) async{
    await req.on("post", (Request req) async{
        String symbol = req.context("urlparam")['symbol'];
        var tpao = await ThirdPartyApp.bySymbol(symbol);
        var tpa;
        if (symbol==null || symbol.isEmpty || !tpao.isPresent){
            giveJSONError(req, 1);
            return;
        } else {
            tpa = tpao.value;
        } // 1:No symbol or app not found 
        var data = tpa.asMap();
        data.remove('id');
        data.remove('owner');
        var result = {
            'ok': true,
            'app': tpa.asMap(),
        };
        var json = req.asJson();
        var token = json['token'];
        if (token!=null){
            if (await tokenCanUse(req, token)){
                var t = (await Token.byToken(token)).value;
                var u = (await User.byUsername(t.username)).value;
                var targetTokenOptional = (await Token.byUsernameSymbol(t.username, symbol));
                var targetToken;
                bool hasTargetToken = false; // for clean meaning
                if (targetTokenOptional.isPresent){
                    targetToken = targetTokenOptional.value;
                    hasTargetToken = true;
                }
                var pg = await u.p(symbol);
                if (pg.acceptedPermissionsString.length <= 0 || hasTargetToken){
                    result['isAppected'] = false;
                } else {
                    result['isAppected'] = true;
                    result['token'] = targetToken.token;
                }
            }
        }
        req.res.ok(result);
    });
}


Future AuthAPI(Request req) async{
    await req.on("post", (Request req) async{
        var json = req.asJson();
        var token = json['token'];
        if ((await tokenCanUse(req, token))){
            var t = (await Token.byToken(token)).value;
            var u = (await User.byUsername(t.username)).value;
            var symbol = json['symbol'];
            var message = json['message'];
            var pAppPG;
            ThirdPartyApp app;
            if (t.parentApplicationId == 0){
                pAppPG = PG_FISHID(t.username);
            } else {
                var appo = await ThirdPartyApp.bySymbol(symbol);
                if (!appo.isPresent){
                    giveJSONError(req, 1);
                    return;
                } else {
                    app = appo.value;
                }
            }
            if (!pAppPG.can("Auth:ThirdParty")){
                giveJSONError(req, -816);
                return;
            }
            await PermissionGroup.acceptBy(t.username, app.symbol, app.permissions);
            var appToken = await u.createToken(message,parentAppID: app.id);
            req.res.ok({
                'ok':true,
                'token': appToken.asMap(),
                'callback':{
                    'method':app.callbackMethod,
                    'url':app.callbackUrl,
                }
            });
        }
    });
}
