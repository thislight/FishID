import 'dart:async';
import '../models/user.dart';
import 'package:webart/web.dart';


Future RegisterAPI(Request req) async{
    await req.on("post", (Request req) async{
        var json = await req.asJson() as Map<String,dynamic>;
        if ((await User.byEmail(json['email'])).isPresent){
            req.res.ok({
                'ok': false,
                'error': 1
            });
        } else if ((await User.byUsername(json['username'])).isPresent){
            req.res.ok({
                'ok': false,
                'error': 2
            });
        } else {
            await User.register(json['email'],json['username'],json['password']);
            req.res.ok({
                'ok': true,
                'email': json['email'],
                'username': json['username']
            });
        }
    });
}