import 'dart:async';
import '../models/token.dart';
import '../models/user.dart';
import 'package:webart/web.dart';
import '../utils/auth.dart';
import '../models/tpa.dart';
import '../models/permission.dart';
import '../models/storage.dart';


Future StorageAPI(Request req) async{
    await req.on("post", (Request req) async{
        var json = req.asJson();
        var token = json['token'];
        if (await tokenCanUse(req, token)){
            String symbol = req.context("urlparam")['appsymbol'];
            String method = req.context("urlparam")['method'];
            var t = (await Token.byToken(token)).value;
            var u = (await User.byUsername(t.username)).value;
            var appo = await ThirdPartyApp.bySymbol(symbol);
            if (!appo.isPresent){
                giveJSONError(req, 1);
                return;
            } // 1: App not found
            var pAppPG;
            if (t.parentApplicationId == 0){
                pAppPG = PG_FISHID(u.username);
            } else {
                var a = (await ThirdPartyApp.byId(t.parentApplicationId)).value;
                pAppPG = u.p(a.symbol);
            }

            switch(method){
                case "get":
                    if (!(pAppPG.can('PrivateStorage:$symbol.get') || pAppPG.can('PrivateStorage:$symbol'))){
                        giveJSONError(req, -816);
                        break;
                    } // -816: permission error
                    var ps = await PrivateStorage.by(u.username, symbol);
                    List keys = json['keys'];
                    var result;
                    if (keys != null){
                        keys.forEach((val){
                            result[val] = ps[val];
                        });
                    } else {
                        result = ps.storage;
                    }
                    req.res.ok({
                        'ok': true,
                        'result': result,
                    });
                    break;
                case "set":
                    if (!(pAppPG.can('PrivateStorage:$symbol.set') || pAppPG.can('PrivateStorage:$symbol'))){
                        giveJSONError(req, -816);
                        break;
                    }
                    var ps = await PrivateStorage.by(u.username, symbol);
                    var data = json['data'];
                    if (!(data is Map)){
                        giveJSONError(req, 2);
                        break;
                    } // 'data' must be a map
                    data.forEach((k,v) => ps[k] = v);
                    await ps.doSync();
                    req.res.ok({
                        'ok': true,
                        'result':ps.storage,
                    });
                    break;
                default:
                    req.res.notFound();
                    break;
            }
        }
    });
}
