import 'dart:async';

dynamic awaitFutureUntilValue(dynamic f) async{
    if (f is Future){
        return await awaitFutureUntilValue(f);
    } else {
        return f;
    }
}