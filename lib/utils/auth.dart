import '../models/token.dart';
import 'dart:async';
import 'package:webart/web.dart' show Request;


Future<bool> tokenCanUse(Request req,String token) async{
    if(token==null || (!(await Token.canUse(token)))){
        req.res.ok({
            'ok':false,
            'err': -817,
            'message': "Token is unavaliable"
        });
        return false;
    } else {
        return true;
    }
}

void giveJSONError(Request req,int errCode){
    req.res.ok({
        'ok': false,
        'err': errCode,
    });
}
