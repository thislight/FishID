

Iterable<int> range(int end,[int start=0]){
    return new Iterable<int>.generate(end-start,(int i) => i);
}
