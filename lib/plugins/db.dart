import 'package:webart/web.dart';
import 'package:mongo_dart/mongo_dart.dart';

class DatabasePlugin extends Plugin{
    @override
    void init(Application app){
        var db = new Db(app.C["database"]);
        app.C["database_instance"] = db;
    }
}
