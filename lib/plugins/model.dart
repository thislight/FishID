import 'dart:mirrors';
import 'package:webart/web.dart';
import '../models/model.dart';


@MirrorsUsed(
    symbols: 'setDatabase',
    targets: 'Model'
)
class ModelPlugin extends Plugin{
    @override
    void init(Application app){
        for (Type model in app.C['models']){
            ClassMirror cls = reflectClass(model);
            cls.superclass.invoke(new Symbol("setDatabase"), [app.C['database_instance']]);
        }
        Model.setDatabase(app.C['database_instance']);
    }
}
