import 'package:webart/web.dart';
import './plugins/db.dart';
import './plugins/model.dart';

Application getApplication(Config c){
    var app = new Application(c);
    app.use(new DatabasePlugin());
    app.use(new ModelPlugin());
    return app;
}