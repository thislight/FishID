import './api/register.dart' show RegisterAPI;
import './api/login.dart' show LoginAPI;
import './api/tpa.dart' show ThirdPartyAppAPI;
import './api/storage.dart' show StorageAPI;
import './api/auth.dart' show AuthAPI,AppAuthInfoAPI;


final Map ROUTING = {
    'api/register': RegisterAPI,
    'api/login': LoginAPI,
    'api/thirdPartyApp/{method}{?appsymbol}': ThirdPartyAppAPI,
    'api/storage/{appsymbol}/{method}': StorageAPI,
    'api/appAuthInfo/{symbol}': AppAuthInfoAPI,
    'api/auth': AuthAPI,
};
