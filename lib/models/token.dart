import './tpa.dart';
import 'dart:async';
import 'package:mongo_dart/mongo_dart.dart' show Db;
import './model.dart';
import './permission.dart';
import 'package:quiver/core.dart';
import 'package:cryptoutils/hash.dart';


DateTime _processExp(int msse){
    if(msse!=null){
        return new DateTime.fromMicrosecondsSinceEpoch(msse);
    } else {
        return null;
    }
}


class Token extends Model{
    String token;
    bool twoStepAuthRequired;
    int parentApplicationId;
    DateTime expiration;
    bool isForever;
    bool avaliable;
    String username;
    String message;

    Token(this.token,this.parentApplicationId,this.isForever,
        this.expiration,this.avaliable,this.username,
        this.twoStepAuthRequired,String message);

    factory Token.fromDocument(Map<String,dynamic> m){
        return new Token(m['token'], m['parentApplicationId'],
                    m['isForever'], _processExp(m['expiration']),
                    m['avaliable'], m['username'],
                    m['twoStepAuthRequired'], m['message']);
    }

    static Future<Token> create(String username,String message,{int parentApplicationId = 0,DateTime expiration}) async{
        var doc = {
            'token': null,
            'username': username,
            'parentApplicationId': parentApplicationId,
            'isForever': null,
            'expiration': expiration,
            'avaliable': true,
            'twoStepAuthRequired': false,
            'message': message,
        };
        var s = "token"+new DateTime.now().microsecondsSinceEpoch.toString()+username;
        String token = new Hash(s).toHex();
        doc['token'] = token;
        if (parentApplicationId == null){
            doc['parentApplicationId'] = 0;
        }
        if (expiration == null){
            doc['isForever'] = true;
        }
        if (parentApplicationId == 0){
            PermissionGroup.acceptBy(username, "FishID", ["*:all"]);
        } else {
            try {
                var app = (await ThirdPartyApp.byId(parentApplicationId)).value;
                await PermissionGroup.acceptBy(username, app.symbol, app.permissions);
                } catch (StateError){
                    doc['avaliable'] = false;
                }
        }
        Map<String,dynamic> back = await (await db()).collection('token').insert(doc);
        return new Token(token, parentApplicationId, back['isForever'],
                    expiration, back['avaliable'], username,
                    back['twoStepAuthRequired'],back['message']);
    }

    static Future<List<Token>> byUsername(String username) async{
        return (await (await db()).collection('token').find({'username':username})
                            .toList()).map((m) => new Token.fromDocument(m))
                            .toList();
    }

    static Future<Optional<Token>> byToken(String token) async{
        return by({
            'token': token
        });
    }

    static Future<Optional<Token>> by(Map params) async{
        var doc = await (await db()).collection('token').findOne(params);
        if (doc == null) return new Optional.absent();
        return new Optional.of(new Token.fromDocument(doc));
    }

    static Future<Optional<Token>> byUsernameSymbol(String username,String symbol) async{
        var tpao = await ThirdPartyApp.bySymbol(symbol);
        if (!tpao.isPresent) return new Optional.absent();
        var tpa = tpao.value;
        return by({
            'username': username,
            'parentApplicationId': tpa.id
        });
    }

    static Future<bool> canUse(String token) async{
        Optional<Token> t = await byToken(token);
        if (
            (!t.isPresent)
            && (t.value.avaliable)
            && (!t.value.twoStepAuthRequired)
            && ((!t.value.isForever) ? (t.value.expiration.isAfter(new DateTime.now())) : true)
            ) return false;
        return true;
    }

    Map<String,dynamic> asMap(){
        var expirationString;
        if (expiration == null){
            expirationString = null;
        } else {
            expirationString = expiration.toIso8601String();
        }
        return {
            'token':token,
            'expiration': expirationString,
            'isForever': isForever,
            'message': message,
            'twoStepAuthRequired': twoStepAuthRequired,
            'avaliable': avaliable,
            'username': username,
            'parentApplicationId': parentApplicationId,
        };
    }

    static Future<Db> db() => Model.db();
}
