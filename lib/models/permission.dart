import 'dart:async';
import './model.dart';
import './user.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:quiver/core.dart';


PermissionGroup PG_FISHID(String username){
    return new PermissionGroup(username, "FishID", ["*:all"]);
}


class Permission{
    String type;
    String permission;

    Permission(this.type,String permission);

    static Optional<Permission> bySingleString(String str){
        if (!str.contains(":")) return new Optional.absent();
        var s = str.split(":");
        return new Optional.of(new Permission(s[0], s[1]));
    }

    List<String> get allSubPermissions => permission.split('.');
}


class PermissionGroup extends Model{
    String username;
    User _user;
    String symbol;
    List<Permission> _acceptedPermissions;
    List<String> acceptedPermissionsString;

    PermissionGroup(this.username,this.symbol,this.acceptedPermissionsString);

    PermissionGroup.withUser(this._user,this.symbol,this.acceptedPermissionsString){
        username = this._user.username;
    }

    Future<User> get user async {
        if (_user == null){
            _user = (await User.byUsername(username)).value;
        }
        return _user;
    }

    List<Permission> get acceptedPermissions {
        if (_acceptedPermissions == null){
            _acceptedPermissions = acceptedPermissionsString.map(
                (s) => Permission.bySingleString(s).value
                ).toList();
        }
        return _acceptedPermissions;
    }

    static Future<Optional<PermissionGroup>> byUsername(String username,String symbol) async{
        Map doc = await (await db()).collection("permission").findOne({
            'username': username,
            'symbol': symbol
        });
        if (doc == null) return new Optional.absent();
        return new Optional.of(new PermissionGroup(username, symbol, doc['acceptPermissions']));
    }

    static Future<Optional<PermissionGroup>> byUser(User user,String symbol){
        return byUsername(user.username, symbol);
    }

    bool can(String s) => acceptedPermissionsString.contains("*:all") || acceptedPermissionsString.contains(s);

    static Future<Map> acceptBy(String username,String symbol,List<String> pers) async{
        return await (await db()).collection("permission").update({
            'username': username,
            'symbol': symbol
        }, {
            'username':username,
            'symbol': symbol,
            'acceptPermissions': pers,
        },upsert: true);
    }

    static Future<Db> db() => Model.db();
}
