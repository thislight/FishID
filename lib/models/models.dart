import './permission.dart';
import './storage.dart';
import './token.dart';
import './tpa.dart';
import './user.dart';

List<dynamic> modelList = [PermissionGroup,PrivateStorage,Token,ThirdPartyApp,User];
