import 'dart:async';
import 'package:mongo_dart/mongo_dart.dart';
import './model.dart';
import './token.dart';
import './permission.dart';
import 'package:crypt/crypt.dart';
import 'package:quiver/core.dart';


Crypt _doCrypt(String key){
    return new Crypt.sha256(key, rounds: 5000, salt: "qwertyuiopasdfgh");
}


class User extends Model{
    String username;
    String email;
    String password;

    User(this.email,this.username,this.password);

    static Future<Optional<User>> byEmail(String email) async{
        Map doc = await (await db()).collection("user").findOne({'email':email});
        if (doc==null) return new Optional.absent();
        return new Optional.of(new User(doc['email'],doc['username'],doc['password']));
    }

    static Future<Optional<User>> byUsername(String username) async{
        Map doc = await (await db()).collection('user').findOne({'username':username});
        if (doc==null) return new Optional.absent();
        return new Optional.of(new User(doc['email'],doc['username'],doc['password']));
    }

    static Future<Optional<User>> login(String user, String password) async{
        var u = await byEmailOrUsername(user);
        if (u.isPresent && u.value.checkPassword(password)){
            return new Optional.of(u.value);
        } else {
            return new Optional.absent();
        }
    }

    static Future<Optional<User>> byEmailOrUsername(String user) async{
        if('@'.allMatches(user).isEmpty){
            return await byUsername(user);
        } else {
            return await byEmail(user);
        }
    }

    static Future<Map> register(String email,String username,String password) async{
        return await (await db()).collection('user').insert({
            'email':email,
            'username':username,
            'password': _doCrypt(password).toString(),
        });
    }

    bool checkPassword(String givenPass){
        return _doCrypt(givenPass).toString() == password;
    }

    Future<Map> changePassword(String newOne) async{
        password = _doCrypt(newOne).toString();
        return await (await db()).collection("user").update({'username':username}, {
            '\$set': {'password': password}
        });
    }

    Future<PermissionGroup> p(String symbol) async{
        var opt = await PermissionGroup.byUser(this, symbol);
        if (opt.isPresent){
            return opt.value;
        } else {
            return new PermissionGroup(username, symbol, []);
        }
    }

    Future<Token> createToken(String message,{int parentAppID:0}) async{
        return await Token.create(username, message, parentApplicationId: parentAppID);
    }

    static Future<Db> db() => Model.db();
}