import 'dart:async';
import 'package:webart/webart.dart';
import 'package:mongo_dart/mongo_dart.dart' show Db,State;


final _logger = getLogger("fish.id.model.Model");


void waitUntilStateIsOpen(Db _db,Completer cpte){
    for (;;){
        if(_db.state == State.OPEN) return cpte.complete(_db);
    }
}


class Model{
    static Db _db;

    static void setDatabase(Db db){
      _db = db;
    }

    static Future<Db> db() async{
        var cpte = new Completer<Db>();
        if (_db == null){
            for(;;){
              if (_db != null) break;
            }
        }
        if (_db.state == State.INIT){
          await _db.open();
          waitUntilStateIsOpen(_db, cpte);
        } else {
          cpte.complete(_db);
        }
        return cpte.future;
    }
}
