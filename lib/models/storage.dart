import './model.dart';
import './user.dart';
import 'dart:async';
import 'package:mongo_dart/mongo_dart.dart';


class PrivateStorage extends Model{
    String username;
    String appSymbol;
    DateTime lastUpdated;
    Map<String,dynamic> storage;

    User _user;
    Future<User> get user async{
        if (_user == null){
            _user = (await User.byUsername(username)).value;
        }
        return _user;
    }

    PrivateStorage(this.username,this.appSymbol,this.storage);

    static Future<PrivateStorage> by(String username,String appSymbol) async{
        var s = new PrivateStorage(username, appSymbol, {});
        await s.doSync();
        return s;
    }

    Future<bool> doSync() async{
        var doc = await (await db()).collection("storage").findOne({
            'username':username,
            'appSymbol':appSymbol
        });
        bool syncRequired = false;
        Map syncData;
        var now = new DateTime.now();
        if (doc == null){
            syncRequired = true;
            syncData = storage;
        } else if (lastUpdated.isBefore(now)){ // Data in DB is new
            storage = doc['storage'];
        } else if (lastUpdated.isAfter(now)){ // Data in DB is old
            syncRequired = true;
            syncData = storage;
        } else if (lastUpdated.isAtSameMomentAs(now)){
            syncRequired = true;
            syncData = storage;
        }
        if (syncRequired){
            await (await db()).collection("storage").update({
                'username':username,
                'appSymbol':appSymbol
            }, {
                '\$set':{
                    'lastUpdated': now,
                    'storage': syncData,
                }
            });
            return true;
        } else return false;
    }

    dynamic operator[](String key) => storage[key];

    void operator[]=(String key,dynamic value) => storage[key] = value;

    static Future<Db> db() => Model.db();
}
