import './model.dart';
import './permission.dart';
import 'dart:async';
import 'package:quiver/core.dart';
import 'package:mongo_dart/mongo_dart.dart';


const int _IDBASE = 300000000;


class ThirdPartyApp extends Model{
    String name;
    String symbol;
    String owner; // Username
    int id;
    String callbackMethod;
    String callbackUrl;
    List<String> permissions;

    List<Permission> get p => permissions.map((s) => Permission.bySingleString(s).value);

    ThirdPartyApp(this.name,this.symbol,this.owner,this.id,this.callbackMethod,this.callbackUrl,this.permissions);

    ThirdPartyApp.fromMap(Map m) : this(
        m['name'],
        m['symbol'],
        m['owner'],
        m['id'],
        m['callbackMethod'],
        m['callbackUrl'],
        m['permissions']
    );

    static Future<ThirdPartyApp> create(String name,String symbol,String owner,String callbackMethod) async{
        var doc = await (await db()).collection("app").insert({
            'name': name,
            'symbol': symbol,
            'owner': owner,
            'id': (await (await db()).collection("app").count()) + _IDBASE,
            'callbackMethod': callbackMethod
        });
        return new ThirdPartyApp.fromMap(doc);
    }
    
    static Future<Optional<ThirdPartyApp>> byId(int id){
        return by({'id':id});
    }

    static Future<Optional<ThirdPartyApp>> bySymbol(String symbol){
        return by({'symbol':symbol});
    }

    static Future<Optional<ThirdPartyApp>> byName(String name){
        return by({'name':name});
    }

    static Future<Optional<ThirdPartyApp>> by(Map s) async{
        var doc = await (await db()).collection("app").findOne(s);
        if (doc == null) return new Optional.absent();
        return new Optional.of(new ThirdPartyApp.fromMap(doc));
    }

    static Future<List<ThirdPartyApp>> byOwner(String owner) async{
        var docs = await (await db()).collection("app").find({'owner':owner});
        return docs.map((Map m) => new ThirdPartyApp.fromMap(m)).toList();
    }

    Future<Map> updateCallbackMethod(String method) async{
        callbackMethod = method;
        return await _update("callbackMethod", method);
    }

    Future<Map> updatePermission(List<String> pers) async{
        permissions = pers;
        return await _update("permissions", pers);
    }

    Future<Map> updateCallbackUrl(String url) async{
        callbackUrl = url;
        return await _update("callbackUrl", url);
    }

    Future<Map> _update(String key, dynamic value) async{
        return await (await db()).collection("app").update({'id':id}, {
            '\$set': {key:value}
        });
    }

    Map<String,dynamic> asMap(){
        return {
            'id': id,
            'name': name,
            'symbol': symbol,
            'owner': owner,
            'callback': {
                'method': callbackMethod,
                'url': callbackUrl,
            },
            'permissions': permissions,
        };
    }

    static Future<Db> db() => Model.db();
}
