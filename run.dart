import './lib/app.dart';
import './lib/routing.dart';
import './lib/models/models.dart' show modelList;
import 'package:webart/web.dart';


void main(){
    var cobj = new Config({
        'database':"mongodb://localhost/fishid",
        'models': modelList,
        'routes': ROUTING
    });
    var app = getApplication(cobj);
    app.start("127.0.0.1", 8089);
}