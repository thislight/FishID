@TestOn('vm')


import './utils.dart';
import '../lib/models/user.dart';
import 'package:test/test.dart';
import 'package:webart/web.dart';
import 'dart:convert' show JSON;

void main(){
    group("Register API",(){
        var application = getApplicationForTest(new Config({}));
        tearDown(() async{
          await sendDropDBCommand(application);
        });
        test("can call and finish register",() async{
            await application.handler(makeRequest("POST", "api/register",JSON.encode({
                "email": "l1589002388@gmail.com",
                "password": "testtesttest",
                "username": "thislight"
            })));
            var user = (await User.byEmail("l1589002388@gmail.com")).value; // Must not absent
            expect(user!=null, equals(true));
            expect(user.checkPassword("testtesttest"), equals(true));
            expect(user.checkPassword("xxxxxxxxxxxx"), equals(false)); // When given password unappected, got a false
            expect(user.username, equals("thislight"));
            // Register anthoer user
            await application.handler(makeRequest("POST", "api/register",JSON.encode({
                "email": "me@thislight.xyz",
                "password": "12345678",
                "username": "rubiconw"
            })));
            var user2 = (await User.byEmail("me@thislight.xyz")).value; // Must not absent
            printOnFailure("${user2.username}:${user2.password}:${user2.email}");
            expect(user2!=null, equals(true),reason: "user is null");
            expect(user2.checkPassword("12345678"), equals(true), reason: "password is not 12345678");
            expect(user2.checkPassword("xxxxxxxxxxx"), equals(false), reason: "password is xxxxxxxxxxx");
            expect(user2.username, equals("rubiconw"));
        });
    });
}
