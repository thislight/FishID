@TestOn("vm")


import 'dart:async';
import "package:test/test.dart";
import './utils.dart';
import '../lib/models/user.dart';
import '../lib/models/token.dart';
import 'package:webart/web.dart';
import 'dart:convert' show JSON;
import 'package:shelf/shelf.dart' as shelf;


Future<Map<String,dynamic>> getBack(shelf.Response res) async => JSON.decode(await res.readAsString());


void main(){
    group("Login API",(){
        var application = getApplicationForTest(new Config({}));
        tearDown(() async{
          await sendDropDBCommand(application);
          await new Future.delayed(const Duration(seconds: 2));
        });
        test("can get token if username and password match",() async{
            await User.register("l1589002388+norecv@gmail.com", "thislight", "testtesttest");
            var res = await application.handler(makeRequest("POST", "api/login", JSON.encode({
                'user': 'thislight',
                'password': 'testtesttest'
            })));
            var back = await getBack(res);
            printOnFailure(back.toString());
            expect(back['ok'], equals(true));
            expect(back['token']!=null, equals(true));
            expect((await Token.byToken(back['token']['token'])).isPresent, equals(true));
        });
        test("can get token if email and password match",() async{
            await User.register("me@thislight.xyz","rubiconw","testtesttest");
            var res = await application.handler(makeRequest("POST", "api/login", JSON.encode({
                'user': 'me@thislight.xyz',
                'password': 'testtesttest'
            })));
            var back = await getBack(res);
            printOnFailure(back.toString());
            expect(back['ok'], equals(true));
            expect(back['token']!=null, equals(true));
            expect((await Token.byToken(back['token']['token'])).isPresent, equals(true));
        });
        test("can not get token if username/email and password not match",() async{
            var res = await application.handler(makeRequest("POST", "api/login", JSON.encode({
                'user':'noname',
                'password': 'hadhjaidakegwegdbagsbdiagsi'
            })));
            var back = await getBack(res);
            printOnFailure(back.toString());
            expect(back['ok'], equals(false));
            expect(back['err'], equals(1));
        });
    });
}
