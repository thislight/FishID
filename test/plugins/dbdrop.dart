import 'package:mongo_dart/mongo_dart.dart';
import 'package:webart/webart.dart';
import '../../lib/models/model.dart';


class DBDropPlugin extends Plugin{
  @override
  void init(Application app){
    app.registerCommandHandler("fish.id.test.dropDatabase", (Command command) async{
      var logger = getLogger("fish.id.plugins.DBDropPlugin");
      Db db = await Model.db();
      logger.shout("Droping Database");
      await db.drop();
      command.completer.complete();
    });
  }
}
