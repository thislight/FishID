import 'package:webart/web.dart';
import '../lib/app.dart';
import '../lib/models/models.dart';
import '../lib/routing.dart';
import 'package:shelf/shelf.dart' as shelf;
import './plugins/dbdrop.dart';
import 'dart:async' show Future;
import 'dart:convert' show JSON;

Application getApplicationForTest(Config c){
    c["database"] = "mongodb://localhost/fishid-test";
    c['models'] = modelList;
    c['routes'] = ROUTING;
    c['debug'] = true;
    var app = getApplication(c);
    app.ready();
    app.use(new DBDropPlugin());
    return app;
}


shelf.Request makeRequest(String method, String target, String body){
    return new shelf.Request(method, Uri.parse("http://example.com/$target"),handlerPath: "/",body: body);
}


Future<Map<String,dynamic>> getBack(shelf.Response res) async => JSON.decode(await res.readAsString());


Future sendDropDBCommand(Application app) async{
  await app.command.send(new Command("fish.id.test.dropDatabase",resultRequired: true));
}
